package com.sg.interview.ingest.stages

import cats.data.Writer
import com.sg.interview.config.DataPipeLineConfig.DataColumn
import com.sg.interview.ingest.DataStageConstants._
import com.sg.interview.ingest.DataUDFs.generateUUID
import com.sg.interview.ingest.models.DataErrorModels.{DataError}
import com.sg.interview.ingest.stages.base.DataStage
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, Dataset, Encoder, SparkSession}
import org.apache.spark.sql.expressions.Window


class AddUniqIdStage(dataCols: List[DataColumn])(implicit spark: SparkSession, encoder: Encoder[DataError])
  extends DataStage[DataFrame] {

  override val stage: String = getClass.getSimpleName

  def apply(errors: Dataset[DataError], data: DataFrame): (Dataset[DataError], DataFrame) = {
    val colOrder = RowKey +: dataCols.map(_.name)
    val withRowKeyDf = data.withColumn(RowKey, lit(generateUUID())).cache()
    withRowKeyDf.show()
    withRowKeyDf.createOrReplaceTempView("df_sensor_vw")
    val resultsDF = spark.sql(
      """with stg_tbl AS(select UqId,sensor, mnemonic, data, timestamp as start_date
         ,CASE WHEN data <> lead(data,1,-1) OVER(PARTITION BY sensor, mnemonic ORDER BY TRUE)  then '1'
          else '0' END as flag
         ,CASE WHEN data <> lead(data,1,-1) OVER(PARTITION BY sensor, mnemonic ORDER BY TRUE)
         then lead(timestamp,1) OVER(PARTITION BY sensor, mnemonic ORDER BY TRUE)
         else lead(timestamp,2) OVER(PARTITION BY sensor, mnemonic ORDER BY TRUE)
         END as end_date from df_sensor_vw)
         select UqId,sensor, mnemonic, data, start_date
         ,end_date from stg_tbl where flag = 1
          """.stripMargin)
    val returnDf = resultsDF.select(colOrder.map(col): _*)
    (spark.emptyDataset[DataError], returnDf)
  }
}
