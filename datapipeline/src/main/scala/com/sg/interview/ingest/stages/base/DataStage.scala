package com.sg.interview.ingest.stages.base

import com.sg.interview.ingest.models.DataErrorModels.{DataError}
import org.apache.spark.sql.Dataset

trait DataStage[T <: Dataset[_]] extends Serializable {
  def apply(errors: Dataset[DataError], data: T): (Dataset[DataError], T)
  def stage: String
}
