package com.sg.interview.ingest

object DataStageConstants {
  val RowKey = "UqId"
  val RowLevelErrorListCol = "row_level_error_list"
  val ErrorSeverity = "ErrorSeverity"
  val WarnSeverity = "WarnSeverity"

}
