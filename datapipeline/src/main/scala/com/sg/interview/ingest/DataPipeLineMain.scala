package com.sg.interview.ingest

import cats.syntax.either._
import com.sg.interview.config.DataPipeLineConfig
import com.sg.interview.config.DataPipeLineConfig.{DatasetConfig, Delimited}
import com.sg.interview.ingest.models.DataErrorModels.DataError
import com.sg.interview.ingest.stages.{AddUniqIdStage, DataTypeValidatorStage}
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.{Level, LogManager}
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object DataPipeLineMain {

  def main(args: Array[String]): Unit = {
    val filePath = args(0)
    val sparkConf = buildSparkConf()

    implicit val spark = SparkSession
      .builder()
      .config(sparkConf)
      .appName("Data Pipeline")
      .master("local[*]")
      .getOrCreate()

    LogManager.getRootLogger.setLevel(Level.WARN)
    spark.sparkContext.setLogLevel("ERROR")

    val fileName = StringUtils.substringAfterLast(filePath, "/")

    val dataSetE =
      for {
        ingestionConfig <- DataPipeLineConfig.getIngestionConfig(
          getClass.getResource("/main_datasets.conf"), "ingestionConfig")
        dataSetIn <- DataPipeLineConfig.findDatasetFromFileName(fileName, ingestionConfig) //Need to figure out how to remove the filename in StructuredStreaming
      } yield dataSetIn

    if (dataSetE.isLeft) {
     sys.error("No matching dataset configuration found for the given file. Please check the filePattern configuration for each dataset")
    }
    dataSetE.map(config => runPipeline(filePath, config))
  }

  private def runPipeline(filePath: String, dataSetConfig: DatasetConfig)(implicit spark: SparkSession) = {
    val sourceRawDf =
      spark
        .read
        .format("csv")
        .option("header", true)
        .option("delimiter", dataSetConfig.fileFormat.asInstanceOf[Delimited].delimiter)
        .load(filePath)

    import DataFrameOps._

    val dataCols = dataSetConfig.columns

    val addUnqIdStage = new AddUniqIdStage(dataCols)
    val dTypeValidatorStage = new DataTypeValidatorStage(dataCols)

    val pipelineStages = List(
      addUnqIdStage,
      dTypeValidatorStage
    )

    val init = (spark.emptyDataset[DataError], sourceRawDf)

    val (errors, processedDf) = pipelineStages.foldLeft(init) { case ((accumErr, df), stage) =>
      stage(accumErr, df)
    }
    processedDf.show(false)
    errors.show(false)
  }

def buildSparkConf(): SparkConf = new SparkConf()
    .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    .set("mapreduce.fileoutputcommitter.marksuccessfuljobs", "false")
    .set("spark.sql.streaming.schemaInference", "true")
    .set("spark.sql.autoBroadcastJoinThreshold", "-1")
    .set("spark.scheduler.mode", "FAIR")
    .set("spark.sql.shuffle.partitions", "1")
    .set("spark.default.parallelism", "1")
    .set("spark.sql.warehouse.dir", "/tmp/sparkwarehose")
    .set("spark.kryoserializer.buffer.max", "1g")
  }
