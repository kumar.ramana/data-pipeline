name := "datapipeline"

version := "0.1"

scalaVersion := "2.11.12"

val sparkVersion = "2.4.0"
val catsVersion = "1.6.0"

//lazy val projectDeps = Seq(
libraryDependencies +=   "org.apache.spark" %% "spark-core" % sparkVersion
libraryDependencies +=   "org.apache.spark" %% "spark-sql" % sparkVersion
libraryDependencies +=   "org.typelevel" %% "cats-core" % catsVersion
libraryDependencies +=   "org.typelevel" %% "cats-kernel" % catsVersion
libraryDependencies +=   "org.typelevel" %% "cats-macros" % catsVersion
libraryDependencies +=   "com.github.pureconfig" %% "pureconfig" % "0.9.2"
libraryDependencies +=   "org.scalatest" %% "scalatest" % "3.0.5" % Test

//)

